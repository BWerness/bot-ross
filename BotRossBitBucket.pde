/**************************************************************************
 * This is the code that runs @JoyOfBotRoss.  It is written in Processing *
 * and requires gifAnimation and twitter4j.  This is sparsely commented,  *
 * and in terms of readability and modifiability some of the worst code   *
 * I've ever written, but since I've gotten a bunch of questions and      *
 * requests for code, I thought I'd post it as is so you can peek behind  *
 * the curtain and see how it runs.                                       *
 *                                                                        *
 * The code is provided as-is, and I'll be unlikely to be able to help.   *
 * It is very brittle code and I can't ensure it will run anywhere except *
 * on my own computer!  I have removed the code that contains my twitter  *
 * information, so it will not post.                                      *
 *                                                                        *
 * A few words about the philosophy.  I really wanted to stick to my story*
 * about this being written by Bob in 1985.  In particular, 1985 was the  *
 * year Perlin fractal noise hit it big, so my first constraint was that  *
 * I wanted the prgrammer of this to not know of Perlin noise.  This is   *
 * also a personal constraint since I think fractal noise is extremely    *
 * over used in the procedural generation community which makes many      *
 * projects look very much the same.  In my day job, I ama probability    *
 * theorist, and so I know the world of randomness is much larger than    *
 * what is normally used.                                                 *
 *                                                                        *
 * So if not fractal noise, then what?  Well for this I took inspiration  *
 * from Bob himself.  He often talked of tapping in simple shapes, and    *
 * he let the brushes and the fact that the paint moved over the canvas   *
 * add character.  So I start also with simple shapes: lines, trianges,   *
 * and circles which are arranged to coarsely imitate the shapes of the   *
 * intended objects and then distort it all with a procedural noise       *
 * function picked to match the idea that you can move paint around on a  *
 * wet canvas.  The code is contained below, but the idea is that you cut *
 * the image into two halves and then shift one a pixel versus the other. *
 * You then repeat that over and over again to simulate paint shifting on *
 * the canvas without mixing (no mudmixers here).                         *
 *                                                                        *
 * Next, is the choice of dithering.  This was done intentionally as a way*
 * to produce "happy accidents."  It would be a lot of work to add        *
 * ways of generating every texture and possible detail like flowers,     *
 * bark, waves of grass, fruit, and any other little detail one could want*
 * to make the scene look good.  From the very beginning, I designed it to*
 * try to add these details via dithering artifacts on a limited color    *
 * palette.  If you play around with dithering for a while, you see that  *
 * when it tried to display a color slightly different from one in its  *
 * palette, it will do so by periodically injecting a very different color*
 * in little waves.  So a color which is slightly more red than its green *
 * will give a green region with little flecks of red like flowers or     *
 * fruit.  A sligtly dark brown will have waves of black running through  *
 * brown like lines of the texture of bark.  By choosing to use dithering *
 * all of these little details just happen by getting lucky with the      *
 * colors and then dithering.  This also matched the fiction, so I was    *
 * happy!                                                                 *
 *                                                                        *
 * Finally I tried, as much as possible, to imitate the order in which Bob*
 * worked on his canvas.  If you watch many episodes, you'll see that he  *
 * worked though intentionally simple means and always in the same general*
 * patters: trees were always a back shadow layer, followed by trunk,     *
 * followed by leaves.  I tried to respect these rules, both since they   *
 * work quite well, and both to try to be authentic to the source.        *
 * This also made it easier to add Bob Ross lines, part of my goal from   *
 * the beginning, since I was doing mostly what Bob was.                  *
 *                                                                        *
 * That's all I have to say about how this was made.  I hope this plus the*
 * code makes it all clearer!  From all of us here I'd like to wish you   *
 * happy painting!                                                        *
 **************************************************************************/

// First we load some of the needed utilities.
import java.util.Arrays;
import gifAnimation.*;
import twitter4j.*;

// To post to twitter, we first write a gif to the HD and then post it.
GifMaker outputGif;

// These control the settings.
boolean post = false;           // This controls it posting
boolean iter = true;           // This controls if it shows the steps or just the finished painting
boolean ditherSwitch = true;   // This turns on and off dithering
boolean noiseOn = true;        // This turns on and off the procedural noise function I use to distort the simple polygonal shapes.
boolean autoMode = true;       // This controls if it automatically shows multiple paintings
boolean deleteOnceDone = true; // This tells it to clean up after itself
boolean projLines = false;     // Project onto the union of lines not the convex hull (Experimental)
int numProj = 7;               // This controls how it projects into the color palette
int psf = 2;                   // The scaling factor for the pixel art
int repFrame = 1;              // Number of repeats per frame

// The Apple-II Color Palette
int[] r1 = new int[] {0, 108,  64, 217, 64, 217, 128, 236, 19,  38, 191,  38, 191, 147, 255};
int[] g1 = new int[] {0,  41,  53,  60, 75, 104, 128, 168, 87, 151, 180, 195, 202, 214, 255};
int[] b1 = new int[] {0,  64, 120, 240,  7,  15, 128, 191, 64, 240, 248,  15, 135, 191, 255};
// The index of the colors I use for the signature.
int sigIdx1 = 5;
int sigBak1 = 1;

// C64 Color Palette
int[] r2 = new int[] {0, 255, 104,  11, 111,  88,  53, 184, 111, 67, 154, 68, 108, 154, 108, 149};
int[] g2 = new int[] {0, 255,  55, 164,  61, 141,  40, 199,  79, 57, 103, 68, 108, 210,  94, 149};
int[] b2 = new int[] {0, 255,  43, 178, 134,  67, 121, 111,  37,  0,  89, 68, 108, 132, 181, 149};
//// The index of the color I use for the signature.
int sigIdx2 = 10;
int sigBak2 = 2;

// Sierra AGI Color Palette
int[] r3 = new int[] {  0,  0,  0,  0,170,170,170,170, 77, 77, 77, 77,255,255,255,255};
int[] g3 = new int[] {  0,  0,170,170,  0,  0, 77,170, 77, 77,255,255, 77, 77,255,255};
int[] b3 = new int[] {  0,170,  0,170,  0,170,  0,170, 77,255, 77,255, 77,255, 77,255};
// The index of the color I use for the signature.
int sigIdx3 = 12;
int sigBak3 = 4;

//CGA
int[] r4 = new int[] {0, 26, 17, 40, 105, 128, 118, 164, 72, 118, 109, 132, 197, 220, 210, 255};
int[] g4 = new int[] {0, 0, 120, 158, 0, 25, 145, 164, 72, 91, 212, 250, 78, 117, 237, 255};
int[] b4 = new int[] {0, 166, 0, 118, 26, 171, 0, 164, 72, 255, 65, 210, 118, 255, 70, 255};
// The index of the color I use for the signature.
int sigIdx4 = 12;
int sigBak4 = 4;

//Pico-8
int[] r5 = new int[] {  0, 126, 29, 95, 171, 0, 255, 95, 131, 255, 194, 0, 255, 41, 255, 255};
int[] g5 = new int[] {  0, 37, 43, 87, 82, 135, 0, 87, 118, 163, 195, 231, 204, 173, 255, 241};
int[] b5 = new int[] {  0, 83, 83, 79, 54, 81, 77, 79, 156, 0, 199, 86, 170, 255, 39, 232};
// The index of the color I use for the signature.
int sigIdx5 = 6;
int sigBak5 = 1;

//Windows 16
int[] r6 = new int[] {0, 128,   0, 128,   0, 128,   0, 192, 128, 255,   0, 255,   0, 255,   0, 255}; 
int[] g6 = new int[] {0,   0, 128, 128,   0,   0, 128, 192, 128,   0, 255, 255,   0,   0, 255, 255};
int[] b6 = new int[] {0,   0,   0,   0, 128, 128, 128, 192, 128,   0,   0,   0, 255, 255, 255, 255};
// The index of the color I use for the signature.
int sigIdx6 = 9;
int sigBak6 = 1;

//Psygnosia from http://androidarts.com/palette/16pal.htm
int[] r7 = new int[] {0, 27, 54, 68, 82, 100, 115, 119, 158, 203, 224, 162,   0,   8,  84,  81}; 
int[] g7 = new int[] {0, 30, 39, 63, 82, 100,  97, 120, 164, 232, 139,  50,  51,  74, 106, 108};
int[] b7 = new int[] {0, 41, 71, 65, 76, 124,  80,  91, 167, 247, 121,  78,   8,  60,   0, 191};
// The index of the color I use for the signature.
int sigIdx7 = 11;
int sigBak7 = 2;

//Gray Scale
int[] r8 = new int[] {0, 51, 102, 153, 204, 255}; 
int[] g8 = new int[] {0, 51, 102, 153, 204, 255}; 
int[] b8 = new int[] {0, 51, 102, 153, 204, 255}; 
// The index of the color I use for the signature.
int sigIdx8 = 5;
int sigBak8 = 0;

// Minecraft stained clay colors
int[] r9 = new int[] {207, 156, 147, 110, 186, 100, 160, 55, 129, 85, 120, 72, 75, 72, 145, 33}; 
int[] g9 = new int[] {172,  79,  85, 105, 133, 114,  77, 39, 102, 90,  71, 58, 51, 80,  61, 18}; 
int[] b9 = new int[] {157,  33, 107, 135,  33,  48,  77, 33,  92, 90,  86, 89, 33, 39,  47, 13}; 
// The index of the color I use for the signature.
int sigIdx9 = 14;
int sigBak9 = 12;

// Minecraft woll colors
int[] r10 = new int[] {221, 219, 179, 107, 177, 65, 208, 64, 154, 46, 126, 46, 79, 53, 150, 25}; 
int[] g10 = new int[] {221, 125, 80, 138, 166, 174, 132, 64, 161, 110, 61, 56, 50, 70, 52, 22}; 
int[] b10 = new int[] {221, 62, 188, 201, 39, 56, 153, 64, 161, 137, 181, 141, 31, 27, 48, 22}; 
// The index of the color I use for the signature.
int sigIdx10 = 14;
int sigBak10 = 12;

// Minecraft colors (woll and clay)
int[] r11 = new int[] {221, 219, 179, 107, 177, 65, 208, 64, 154, 46, 126, 46, 79, 53, 150, 25, 207, 156, 147, 110, 186, 100, 160, 55, 129, 85, 120, 72, 75, 72, 145, 33}; 
int[] g11 = new int[] {221, 125, 80, 138, 166, 174, 132, 64, 161, 110, 61, 56, 50, 70, 52, 22, 172,  79,  85, 105, 133, 114,  77, 39, 102, 90,  71, 58, 51, 80,  61, 18}; 
int[] b11 = new int[] {221, 62, 188, 201, 39, 56, 153, 64, 161, 137, 181, 141, 31, 27, 48, 22, 157,  33, 107, 135,  33,  48,  77, 33,  92, 90,  86, 89, 33, 39,  47, 13}; 
// The index of the color I use for the signature.
int sigIdx11 = 14;
int sigBak11 = 12;

// Minecraft map colors
int[] r12 = new int[] {127,247,199,255,160,167,  0,255,164,151,112, 64,143,255,216,178,102,229,127,242,76,153, 76,127, 51,102,102,153,25,250, 92, 74,  0,129,112}; 
int[] g12 = new int[] {178,233,199,  0,160,167,124,255,168,109,112, 64,119,252,127, 76,153,229,104,127,76,153,127, 63, 76, 76,127,51, 25,238,219,128,217, 86,  2}; 
int[] b12 = new int[] { 56,163,199,  0,255,167,  0,255,184, 77,112,255, 72,245, 51,216,216, 51, 25,165,76,153,153,178,178, 51, 51,51, 25, 77,213,255, 58, 49,  0}; 
// The index of the color I use for the signature.
int sigIdx12 = 3;
int sigBak12 = 34;

// Gameboy color
int[] r13 = new int[] {175,121,34,8}; 
int[] g13 = new int[] {203,170,111,41}; 
int[] b13 = new int[] {70,109,95,85}; 
// The index of the color I use for the signature.
int sigIdx13 = 0;
int sigBak13 = 3;

// Dawnbreaker 32
int[] r14 = new int[] {0,34,69,102,143,223,217,238,251,153,106,55,75,82,50,63,48,91,99,95,203,255,155,132,105,89,118,172,217,215,143,138}; 
int[] g14 = new int[] {0,32,40,57,86,113,160,195,242,229,190,148,105,75,60,63,96,110,155,205,219,255,173,126,106,86,66,50,87,123,151,111}; 
int[] b14 = new int[] {0,52,60,49,59,38,102,154,54,80,48,110,47,36,57,116,130,225,255,228,252,255,183,135,106,82,138,50,99,186,74,48}; 
// The index of the color I use for the signature.
int sigIdx14 = 27;
int sigBak14 = 3;

int[] r;
int[] g;
int[] b;
int sigIdx;
int sigBak;

// These are all the layers that it builds and composites into the final image
PImage in;
PImage front;
PImage water;
PImage disp;
PImage sun;
PImage clouds;
PImage reflCloud;
PImage mountains;
PImage reflMtn;
PImage farTrees;
PImage grassyAreas;
PImage contactPaper;
PImage finalTree;
boolean addSig;

//this holds the custom pixel font I use
PFont pixelFont;

void settings() {
  size(253*psf, psf*(144+16));
}

// These control the randomness
float globalRandScale;
int newSeed = 608981558;

void setup() {
  //newSeed = abs((int) System.currentTimeMillis());
  initIn();
  initOut();
  
  pixelFont = loadFont("4TallWithDrop-" + psf*16 + ".vlw");
  textFont(pixelFont, psf*16);
  
  frame.setBackground(new java.awt.Color(255,255,255));
  
  if (post) surface.setVisible(false);
  noSmooth();
}

// Initialize all the layers
void initIn() {
  in = createImage(233,144,RGB);
  front = createImage(233,144,ARGB);
  water = createImage(233,144,ARGB);
  clouds = createImage(2*233,2*144,ARGB);
  reflCloud = createImage(2*233,2*144,ARGB);
  sun = createImage(1,1,ARGB);
  mountains = createImage(2*233,2*144,ARGB);
  reflMtn = createImage(2*233,2*144,ARGB);
  farTrees = createImage(233,144,ARGB);
  grassyAreas = createImage(233,144,ARGB);
  contactPaper = createImage(233,144,ARGB);
  finalTree = createImage(233,144,ARGB);
  
  // fill with liquidwhite
  in.loadPixels();
  for (int i = 0; i < in.pixels.length; i++) {
    in.pixels[i] = color(255);
  }
  in.updatePixels();
}

// This does a raw pixels scale
void scaleIm() {
  disp = createImage(psf*in.width,psf*in.height,RGB);
  disp.loadPixels();
  in.loadPixels();
  for (int i = 0; i < disp.pixels.length; i++) {
    int x = (i%disp.width)/psf;
    int y = (i/disp.width)/psf;
    disp.pixels[i] = in.pixels[x+in.width*y];
  }
  in.updatePixels();
  disp.updatePixels();
}

//*** PROCEDURAL PRIMATIVES ***

// This is the main source of procesdural randomness.  Given an image in, it
// iteratively cuts the image at a random location (horizontally with probability
// pHor, vertically with probability 1-pHor) and then slides one side against
// the other.  It repeats that n times and saves the result back to in.  Aside
// from boundary issues, this is a bijective source of noise meaning that 
// every pixel in the input image ends up in the output image and vice versa.
void wiggle(PImage in, int n, float pHor) {
  n = int(n*globalRandScale);
  if (!noiseOn) n = 0;
  
  int[][] xc = new int[in.width][in.height];
  int[][] yc = new int[in.width][in.height];
  
  // initialize it all
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      xc[i][j] = i;
      yc[i][j] = j;
    }
  }
  
  for (int t = 0; t < n; t++) {
    if (random(1.0) < pHor) {
      int dir = 2*int(random(2))-1;
      int side = int(random(2));
      int p = int(random(in.height+1));
      for (int i = 0; i < in.width; i++) {
        for (int j = 0; j < in.height; j++) {
          if ((side == 0) && yc[i][j] < p) xc[i][j] += dir;
          if ((side == 1) && yc[i][j] >= p) xc[i][j] += dir;
        }
      }
    }
    else {
      int dir = 2*int(random(2))-1;
      int side = int(random(2));
      int p = int(random(in.width+1));
      for (int i = 0; i < in.width; i++) {
        for (int j = 0; j < in.height; j++) {
          if ((side == 0) && xc[i][j] < p) yc[i][j] += dir;
          if ((side == 1) && xc[i][j] >= p) yc[i][j] += dir;
        }
      }
    }
  }
  
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      xc[i][j] = max(min(xc[i][j],in.width-1),0);
      yc[i][j] = max(min(yc[i][j],in.height-1),0);
    }
  }
  PImage temp = createImage(in.width,in.height,ARGB);
  temp.copy(in,0,0,in.width,in.height,0,0,in.width,in.height);
  
  in.loadPixels();
  temp.loadPixels();
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      in.pixels[i+in.width*j] = temp.pixels[xc[i][j]+in.width*yc[i][j]];
    }
  }
  temp.updatePixels();
  in.updatePixels();
}

// The same as above but you can fix a location to not move
void wiggle(PImage in, int n, float pHor, int fixI, int fixJ) {
  n = int(n*globalRandScale);
  if (!noiseOn) n = 0;

  int[][] xc = new int[in.width][in.height];
  int[][] yc = new int[in.width][in.height];
  
  // initialize it all
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      xc[i][j] = i;
      yc[i][j] = j;
    }
  }
  
  for (int t = 0; t < n; t++) {
    if (random(1.0) < pHor) {
      int dir = 2*int(random(2))-1;
      int side = int(random(2));
      int p = int(random(in.height+1));
      for (int i = 0; i < in.width; i++) {
        for (int j = 0; j < in.height; j++) {
          if ((side == 0) && yc[i][j] < p) xc[i][j] += dir;
          if ((side == 1) && yc[i][j] >= p) xc[i][j] += dir;
        }
      }
    }
    else {
      int dir = 2*int(random(2))-1;
      int side = int(random(2));
      int p = int(random(in.width+1));
      for (int i = 0; i < in.width; i++) {
        for (int j = 0; j < in.height; j++) {
          if ((side == 0) && xc[i][j] < p) yc[i][j] += dir;
          if ((side == 1) && xc[i][j] >= p) yc[i][j] += dir;
        }
      }
    }
  }
  
  int sI = fixI - xc[fixI][fixJ];
  int sJ = fixJ - yc[fixI][fixJ];
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      xc[i][j] += sI;
      yc[i][j] += sJ;
    }
  }
  
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      xc[i][j] = max(min(xc[i][j],in.width-1),0);
      yc[i][j] = max(min(yc[i][j],in.height-1),0);
    }
  }
  PImage temp = createImage(in.width,in.height,ARGB);
  temp.copy(in,0,0,in.width,in.height,0,0,in.width,in.height);
  
  in.loadPixels();
  temp.loadPixels();
  for (int i = 0; i < in.width; i++) {
    for (int j = 0; j < in.height; j++) {
      in.pixels[i+in.width*j] = temp.pixels[xc[i][j]+in.width*yc[i][j]];
    }
  }
  temp.updatePixels();
  in.updatePixels();
}

//*** SKY CODE ***

// The sky is a simple gradient distorted by the noise--- that's it!
PImage skyGradient(int w, int h) {
  PImage out = createImage(w,h,ARGB);
  
  //skycolor
  int r = skyColor[0];
  int g = skyColor[1];
  int b = skyColor[2];
  
  out.loadPixels();
  
  float skyPow = 0.5 + random(0.5);
  
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = 1.0 - ((float) i)/(out.pixels.length-1);
    frac = pow(frac,skyPow);
    out.pixels[i] = color(r,g,b,frac*255);
  }
  out.updatePixels();
  
  wiggle(out,int(random(40000))+10000,0.75);
  
  out.loadPixels();
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = 1.0 - ((float) i)/(out.pixels.length-1);
    frac = min(1.0,frac*4);
    frac = frac*frac*(3-2*frac);
    out.pixels[i] = color(red(out.pixels[i]),green(out.pixels[i]),blue(out.pixels[i]),alpha(out.pixels[i])*frac);
  }
  out.updatePixels();
  
  out.filter(BLUR,1);
  
  return out;
}

// The sun is just a blurred circle.
PImage sunGradient(int w, int h) {
  PGraphics temp = createGraphics(w,h);
  temp.noSmooth();
  temp.beginDraw();
  temp.noStroke();
  temp.fill(sunColor[0],sunColor[1],sunColor[2]);
  temp.ellipse(w/2,h/2,w/3,h/3);
  temp.filter(BLUR,1+int(random(5)));
  temp.endDraw();
  
  return temp;
}

// a cloud is just a gradient triangle.
PImage cloud(int w, int h) {
  PGraphics temp = createGraphics(w,h);
  temp.noSmooth();
  temp.beginDraw();
  temp.noStroke();
  temp.fill(cloudColor[0],cloudColor[1],cloudColor[2]);
  
  temp.triangle(0,h,w/2,0,w,h);
  
  temp.loadPixels();
  for (int i = 0; i < temp.pixels.length; i++) {
    float frac = 1-((float) i)/(temp.pixels.length-1);
    frac = frac*frac*(3-2*frac);
    temp.pixels[i] = color(red(temp.pixels[i]),green(temp.pixels[i]),blue(temp.pixels[i]),alpha(temp.pixels[i])*frac); 
  }
  temp.updatePixels();
  
  temp.endDraw();
  
  return temp; 
}

//*** WATER CODE ***

// The water is just a bijectively wiggled gradient, alond with a term based on moire patterns that
// generates vaguely wave shaped patterns.  The moire like patterns are useful since they hint at
// multiple directions of the waves and so look ok no matter where the land goes.
PImage waterGradient(int w, int h) {
  PImage out = createImage(w,h,ARGB);
  
  //skycolor
  int r = skyColor[0];
  int g = skyColor[1];
  int b = skyColor[2];
  
  out.loadPixels();
  
  float reflectPower = 0.25+random(0.5);
  float waveLength = random(2048);
  float waveStrength = random(1.0);
  int waveMode = int(random(3));

  for (int i = 0; i < out.pixels.length; i++) {
    float x = i%out.width;
    x -= out.width/2 + waterShift;
    x = abs(x);
    float frac = pow(x/(out.width/2),reflectPower);
    switch (waveMode) {
      case 0: frac *= waveStrength*((1.0*i)/out.pixels.length)*(1+cos((waveLength*out.pixels.length)/i))/2 + (1-waveStrength*((1.0*i)/out.pixels.length)); break;
      case 1: frac *= waveStrength*((1.0*i)/out.pixels.length)*(((waveLength*out.pixels.length)/i)%1) + (1-waveStrength*((1.0*i)/out.pixels.length)); break;
      case 2: frac *= waveStrength*((1.0*i)/out.pixels.length)*(1 - ((waveLength*out.pixels.length)/i)%1) + (1-waveStrength*((1.0*i)/out.pixels.length)); break;
    }
    out.pixels[i] = color(r,g,b,frac*255);
  }
  out.updatePixels();
  
  wiggle(out,int(random(1000)),1.0);
  
  out.loadPixels();
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = ((float) i)/(out.pixels.length-1);
    frac = min(1.0,frac*10);
    frac = frac*frac*(3-2*frac);
    out.pixels[i] = color(red(out.pixels[i]),green(out.pixels[i]),blue(out.pixels[i]),alpha(out.pixels[i])*frac);
  }
  out.updatePixels();
  
  return out;
}

//*** MOUNTAIN CODE ***

// A mountain is just a gradient triangle
PImage singlePeak(int w, int h) {
  PGraphics out = createGraphics(w,h);
  float tip = random(0.5)+0.25;
  float split = random(1.0);
  out.noSmooth();
  out.beginDraw();
  out.noStroke();
  out.fill(mtnLightColor[0],mtnLightColor[1],mtnLightColor[2]);
  out.triangle(split*w,h,tip*w,0,w,h);
  out.fill(mtnShadColor[0],mtnShadColor[1],mtnShadColor[2]);
  out.triangle(0,h,tip*w,0,split*w,h);
  
  out.loadPixels();
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = 1-((float) i)/(out.pixels.length-1);
    frac = min(1.0,frac*1);
    frac = frac*frac*(3-2*frac);
    out.pixels[i] = color(red(out.pixels[i]),green(out.pixels[i]),blue(out.pixels[i]),alpha(out.pixels[i])*frac); 
  }
  out.updatePixels();
  
  out.endDraw();
  
  return out;
}

//*** GRASS CODE ***
// The grassy areas are just a bunch of triangles stacked with a water line drawn in and some transparency which
// are then distorted by the noise function.
PImage grassyArea(int w, int h, float s) {
  PGraphics out = createGraphics(w,h); 
  out.noSmooth();
  out.beginDraw();
  out.noStroke();
  
  //dirt base
  out.fill(dirtColor[0],dirtColor[1],dirtColor[2]);
  out.triangle(out.width/4,out.height/2,out.width,0,out.width,out.height);
   
  // grass layers
  for (int i =0; i < 6; i++) {
    out.fill(s*grassColor[0]+(1-s)*mtnShadColor[0]+int(random(32))-int(random(32)),s*grassColor[1]+(1-s)*mtnShadColor[1]+int(random(32))-int(random(32)),s*grassColor[2]+(1-s)*mtnShadColor[2]+int(random(32))-int(random(32)),64);
    float grassLevel = 0.5+random(0.5);
    float grassHeight = 0.5+random(0.75);
    out.triangle(int(grassLevel*out.width/4+(1-grassLevel)*out.width),int(grassLevel*out.height/2),out.width,0,out.width,int(grassHeight*out.height));
  }
  
  out.loadPixels();
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = 1-((float) i)/(out.pixels.length-1);
    frac = min(1.0,frac*2);
    frac = frac*frac*(3-2*frac);
    out.pixels[i] = color(red(out.pixels[i]),green(out.pixels[i]),blue(out.pixels[i]),alpha(out.pixels[i])*frac); 
  }
  out.updatePixels();
  
  out.stroke(waterLineColor[0],waterLineColor[1],waterLineColor[2]);
  out.line(out.width/4,out.height/2,out.width,(3*out.height)/4);
  
  wiggle(out,(out.width*out.height)/5+int(random((out.width*out.height)/50)),0.97,(3*out.width)/4,out.height/2);
  out.endDraw();
  return out;
}

// One of those Java aux. classes
class Pt implements Comparable<Pt>{
  int xc;
  int yc;
  
  Pt (int xc, int yc) {
    this.xc = xc;
    this.yc = yc;
  }
  
  public int compareTo(Pt y) {
    return this.yc - y.yc;
  }
}

// Generates with a 100px vertical buffer area to not chop off the plants
// This puts plants on top of a grassy area.  It picks points, checks to see if they are on the land,
// and then places tress where there are on land.
PImage grassyAreaWithPlants(int w, int h, float s) {
  int num = int(random(1.0)*treeNum);
  
  PImage out = createImage(w,h+100,ARGB);
  PImage temp = createImage(w,h+100,ARGB);
  PImage island = grassyArea(w,h/2,s);
  out.blend(island,0,0,w,h/2,0,h/4+100,w,h/2,BLEND);
  temp.blend(island,0,0,w,h/2,0,h/4+100,w,h/2,BLEND);
  temp.loadPixels();
  
  Pt[] c = new Pt[num];
  
  for (int i = 0; i < num; i++) {
    do {
      c[i] = new Pt(min(max(int(out.width*(1-pow(random(1.0),2.0))),0),out.width-1),min(max(int(out.height*random(1.0)),0),out.height-1));
    } while (alpha(temp.pixels[c[i].xc+out.width*c[i].yc]) != 255);
  }
  
  Arrays.sort(c);
  
  for (int i = 0; i < num; i++) {
    int tw = int((h/2)*(1-pow(random(1.0),1.0/2.0)))+1; //this generates a beta(1,3) (modified for beta(1,2) to make it less extreme)
    PImage tree = bush(tw,tw,s);
    out.blend(tree,0,0,tree.width,tree.height,c[i].xc-tree.width/2,c[i].yc-tree.height,tree.width,tree.height,BLEND);
  }
  temp.updatePixels();
  
  return out;
}

// Generates with a 100px vertical buffer area to not chop off the plants
// This puts plants on top of a grassy area.  It picks points, checks to see if they are on the land,
// and then places tress and bushes where there are on land.
PImage grassyAreaWithPlantsAndBushes(int w, int h, float s) {
  float tProb = random(1.0)*(treeNum/1000.0);
  
  PImage out = createImage(w,h+100,ARGB);
  PImage temp = createImage(w,h+100,ARGB);
  PImage island = grassyArea(w,h/2,s);
  out.blend(island,0,0,w,h/2,0,h/4+100,w,h/2,BLEND);
  temp.blend(island,0,0,w,h/2,0,h/4+100,w,h/2,BLEND);
  temp.loadPixels();
  
  Pt[] c = new Pt[2000];
  
  for (int i = 0; i < 2000; i++) {
    do {
      c[i] = new Pt(min(max(int(out.width*(1-pow(random(1.0),2.0))),0),out.width-1),min(max(int(out.height*pow(random(1.0),1.0)),0),out.height-1));
    } while (alpha(temp.pixels[c[i].xc+out.width*c[i].yc]) <192);
  }
  
  Arrays.sort(c);
  
  for (int i = 0; i < 1000; i++) {
    if (random(1.0) < tProb) {
      int tw = int((h/2)*(1-pow(random(1.0),1.0/2.0)))+1; //this generates a beta(1,3) (modified for beta(1,2) to make it less extreme)
      PImage tree = bush(tw,tw,s);
      out.blend(tree,0,0,tree.width,tree.height,c[2*i].xc-tree.width/2,c[2*i].yc-tree.height,tree.width,tree.height,BLEND);
    }
    
    if (random(1.0) < bushiness) {
      int bw = (h/15)+1;
      PImage bushy = littleBush(bw,bw,s);
      out.blend(bushy,0,0,bushy.width,bushy.height,c[2*i+1].xc-bushy.width/2,c[2*i+1].yc-bushy.height,bushy.width,bushy.height,BLEND);
    }
  }
  temp.updatePixels();
  
  return out;
}

//*** DISTANT TREES ***
// This generates just the little far away strokes that hint at trees on the horizon.
PImage farTrees(int w, int h) {
  float foggyness = 0.25+random(0.75);
  float reflectivity = random(1.0);
  PImage out = createImage(w,h,ARGB);
  
  out.loadPixels();
  for (int i = 0; i < w; i++) {
    int hr = int(random((h/2)+1));
    for (int j = 0; j < h; j++) {
      if ((j < (h/2+1)) && (j >= hr)) {
        float per = ((float) j)/(h/2);
        out.pixels[i+j*w] = color(mtnShadColor[0],mtnShadColor[1],mtnShadColor[2], int(per*255*foggyness));
      }
      else if (j == (h/2+1)) {
        out.pixels[i+j*w] = color(waterLineColor[0],waterLineColor[1],waterLineColor[2],int(255*foggyness));
      }
      else if ((j > (h/2+1)) && (j < (h-hr))) {
        float per = 1.0 - ((float) (j-h/2))/(h/2);
        out.pixels[i+j*w] = color(mtnShadColor[0],mtnShadColor[1],mtnShadColor[2], int(per*128*foggyness*reflectivity));
      }
    }
  }
  out.updatePixels();
  
  return out;
}

//*** PLANTS ***
// A bush is what I call a tree.  It picks between pine and deciduous.
PImage bush(int w, int h, float s) {
  if (random(1.0) < pinePercent) return pine(w,h,s);
  else return deciduous(w,h,s);
}

// This draws a little bush.  There is a background circle with gradient which is distorted by noise,
// and then a foreground circle with gradient which is distorted by noise again.
PImage littleBush(int w, int h, float s) {
  PGraphics out = createGraphics(w,h); 
  out.noSmooth();
  
  out.beginDraw();
  out.noStroke();
  out.fill(s*backLeafColor[0]+(1-s)*mtnShadColor[0],s*backLeafColor[1]+(1-s)*mtnShadColor[1],s*backLeafColor[2]+(1-s)*mtnShadColor[2],128);
  
  out.ellipse(out.width/2,(3*out.height)/4, out.width/2, out.width/2);
  wiggle(out,(out.width*out.height)/5+int(random((out.width*out.height)/10)),0.5);
  
  out.fill(s*bushColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*bushColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*bushColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)));
  
  out.ellipse(out.width/2,(3*out.height)/4, out.width/2, out.width/2);
  
  wiggle(out,(out.width*out.height)/5+int(random((out.width*out.height)/10)),0.5,out.width/2,out.height-1);
  
  out.loadPixels();
  for (int i = 0; i < out.pixels.length; i++) {
    float frac = 1.0 - ((float) i)/(out.pixels.length-1);
    frac = min(1.0,frac*2);
    frac = frac*frac*(3-2*frac);
    out.pixels[i] = color(red(out.pixels[i]),green(out.pixels[i]),blue(out.pixels[i]),alpha(out.pixels[i])*frac);
  }
  out.updatePixels();
  
  out.endDraw();
  return out;
}

// This draws a simple deciduous tree.  It picks between dead or alive trees, sparklers or not.  A tree is nothing but some
// background circles, some lines for trunks, and then some foreground circles.  When the tree is large, the circles are replaced
// with slightly more detailed blobs with twigs.
PImage deciduous(int w, int h, float s) {
  PGraphics out = createGraphics(w,h); 
  out.noSmooth();
  
  boolean isDead = (random(1.0) < deadProb);
  boolean isSparkler = (random(1.0) < sparklerProb);
  
  out.beginDraw();
  out.noStroke();
  out.fill(s*backLeafColor[0]+(1-s)*mtnShadColor[0],s*backLeafColor[1]+(1-s)*mtnShadColor[1],s*backLeafColor[2]+(1-s)*mtnShadColor[2],128);
  int numTap = int(random(5))+15;
  
  if (!isDead){
    for (int i = 0; i < numTap/2; i++) {
      int rad = int(random(out.width/8))+out.width/8+1;
      if (rad < 20) {
        out.ellipse(out.width/2+int(random(out.width/4)-random(out.width/4)),out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8, rad, rad);
      }
      else {
        PImage tuft = leafyCircle(rad,rad,random(TWO_PI),false,isSparkler, s);
        out.image(tuft,out.width/2+int(random(out.width/4)-random(out.width/4))-rad/2,out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8-rad/2, rad, rad);
      }
    }
  }
  
  int numTwig = 2+int(random(2));
  out.stroke(s*bushTrunkColor[0]+(1-s)*mtnShadColor[0],s*bushTrunkColor[1]+(1-s)*mtnShadColor[1],s*bushTrunkColor[2]+(1-s)*mtnShadColor[2]);
  
  for (int i = 0; i < numTwig; i++) {
    out.strokeWeight(1+h/100+int(random(h/100.0)));
    out.line(out.width/2+int(random(out.width/5)-random(out.width/5)),out.height,out.width/2+int(random(out.width/4)-random(out.width/4)),out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8);
  }
  
  if (!isDead) {
    out.noStroke();
    if (isSparkler) out.fill(s*sparklerColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*sparklerColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*sparklerColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    else out.fill(s*leafColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*leafColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*leafColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    
    for (int i = 0; i < numTap; i++) {
      int rad = int(random(out.width/8))+out.width/16+1;
      if (rad < 20) {
        out.ellipse(out.width/2+int(random(out.width/4)-random(out.width/4)),out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8, rad, rad);
      }
      else {
        PImage tuft = leafyCircle(rad,rad,random(TWO_PI),true,isSparkler, s);
        out.image(tuft,out.width/2+int(random(out.width/4)-random(out.width/4))-rad/2,out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8-rad/2, rad, rad);
      }
    }

    out.fill(s*backLeafColor[0]+(1-s)*mtnShadColor[0],s*backLeafColor[1]+(1-s)*mtnShadColor[1],s*backLeafColor[2]+(1-s)*mtnShadColor[2],128);
    for (int i = 0; i < numTap/4; i++) {
      int rad = int(random(out.width/8))+out.width/16+1;
      if (rad < 20) {
        out.ellipse(out.width/2+int(random(out.width/4)-random(out.width/4)),out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8, rad, rad);
      }
      else {
        PImage tuft = leafyCircle(rad,rad,random(TWO_PI),false,isSparkler, s);
        out.image(tuft,out.width/2+int(random(out.width/4)-random(out.width/4))-rad/2,out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8-rad/2, rad, rad);
      }
    }

    if (isSparkler) out.fill(s*sparklerColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*sparklerColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*sparklerColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    else out.fill(s*leafColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*leafColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*leafColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    
    for (int i = 0; i < numTap/2; i++) {
      int rad = int(random(out.width/8))+out.width/16+1;
      if (rad < 20) {
        out.ellipse(out.width/2+int(random(out.width/4)-random(out.width/4)),out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8, rad, rad);
      }
      else {
        PImage tuft = leafyCircle(rad,rad,random(TWO_PI),true,isSparkler, s);
        out.image(tuft,out.width/2+int(random(out.width/4)-random(out.width/4))-rad/2,out.width/2+int(random(out.width/4)-random(out.width/4))+out.width/8-rad/2, rad, rad);
      }
    }
  }
  
  wiggle(out,(out.width*out.height)/40+int(random((out.width*out.height)/100)),0.5);
  
  out.endDraw();
  return out;
}

// This generates a blob with twigs which replaces a circle if you are close to a deciduous tree.
PImage leafyCircle(int w, int h, float t, boolean front, boolean sparkler, float s) {
  PGraphics out = createGraphics(w,h);
  
  int numTwig = 1+int(random(2));
  out.noSmooth();
  out.beginDraw();
  
  if (front) {
    if (sparkler) {
      out.stroke(s*bushTrunkColor[0]+(1-s)*mtnShadColor[0],s*bushTrunkColor[1]+(1-s)*mtnShadColor[1],s*bushTrunkColor[2]+(1-s)*mtnShadColor[2]);
      out.fill(s*sparklerColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*sparklerColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*sparklerColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    }
    else {
      out.stroke(s*bushTrunkColor[0]+(1-s)*mtnShadColor[0],s*bushTrunkColor[1]+(1-s)*mtnShadColor[1],s*bushTrunkColor[2]+(1-s)*mtnShadColor[2]);
      out.fill(s*leafColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*leafColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*leafColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
    }
  }
  else {
    out.stroke(s*backLeafColor[0]+(1-s)*mtnShadColor[0],s*backLeafColor[1]+(1-s)*mtnShadColor[1],s*backLeafColor[2]+(1-s)*mtnShadColor[2]);
    out.fill(s*backLeafColor[0]+(1-s)*mtnShadColor[0],s*backLeafColor[1]+(1-s)*mtnShadColor[1],s*backLeafColor[2]+(1-s)*mtnShadColor[2],128);
  }
  
  out.strokeWeight(1);
  for (int i = 0; i < numTwig; i++) {
    out.line((w/2)+(w/2)*cos(t),(h/2)+(h/2)*sin(t),(w/2)+(w/2)*(random(2.0)-1),(h/2)+(h/2)*(random(2.0)-1));
  }
  
  int numPuff = 10;
  out.noStroke();
  for (int i = 0; i < numPuff; i++) {
   out.triangle((w/2)+(w/2)*(random(2.0)-1),(h/2)+(h/2)*(random(2.0)-1),
            (w/2)+(w/2)*(random(2.0)-1),(h/2)+(h/2)*(random(2.0)-1),
            (w/2)+(w/2)*(random(2.0)-1),(h/2)+(h/2)*(random(2.0)-1));
  }
  wiggle(out,(w*w)/20,0.5);
  out.endDraw();
  
  return out;
}

// This generates a pine tree.  First some background triangles, then a trunk, then some foreground triangles.
// If it is large, it adds some sticks for branches.
PImage pine(int w, int h, float s) {
  PGraphics out = createGraphics(w,h); 
  out.noSmooth();
  out.beginDraw();
  out.noStroke();
  out.fill(s*pineBackLeafColor[0]+(1-s)*mtnShadColor[0],s*pineBackLeafColor[1]+(1-s)*mtnShadColor[1],s*pineBackLeafColor[2]+(1-s)*mtnShadColor[2],128);
  
  float ws = 0.5+random(0.5);
  
  int numTap = 30+int(random(10));
  for (int i = 0; i < numTap; i++) {
    float hf1 = random(1.0);
    int h1 = int(h*hf1);
    float wf1 = random(1.0)-0.5;
    int w1 = int(w/2 + w*hf1*wf1*ws);
    float hf2 = hf1*0.9;
    int h2 = int(h*hf2);
    float wf2 = -wf1;
    int w2 = int(w/2 + w*hf2*wf2*ws);
    float hf3 = hf1*(0.8+random(0.2));
    int h3 = int(h*hf3);
    float wf3 = 0;
    int w3 = int(w/2 + w*hf3*wf3*ws);
    out.triangle(w1,h1,w2,h2,w3,h3);
  }
  
  out.stroke(s*pineTrunkColor[0]+(1-s)*mtnShadColor[0],s*pineTrunkColor[1]+(1-s)*mtnShadColor[1],s*pineTrunkColor[2]+(1-s)*mtnShadColor[2]);
  out.strokeWeight(1+(h/80)+int(random(h/80)));
  out.line(out.width/2+int(random(out.width/16))-int(random(out.width/16)),out.height,out.width/2,0);
  
  out.noStroke();
  out.fill(s*pineLeafColor[0]+(1-s)*mtnShadColor[0] + int(random(16)) - int(random(16)),s*pineLeafColor[1]+(1-s)*mtnShadColor[1] + int(random(16)) - int(random(16)),s*pineLeafColor[2]+(1-s)*mtnShadColor[2] + int(random(16)) - int(random(16)),128);
  
  for (int i = 0; i < 2*numTap; i++) {
    float hf1 = random(1.0);
    int h1 = int(h*hf1);
    float wf1 = random(1.0)-0.5;
    int w1 = int(w/2 + w*hf1*wf1*ws);
    float hf2 = hf1*0.9;
    int h2 = int(h*hf2);
    float wf2 = -wf1;
    int w2 = int(w/2 + w*hf2*wf2*ws);
    float hf3 = hf1*(0.8+random(0.2));
    int h3 = int(h*hf3);
    float wf3 = 0;
    int w3 = int(w/2 + w*hf3*wf3*ws);
    
    if ((w > 100) && (random(1.0) < 0.4)) { // A few sticks show
      out.stroke(s*pineTrunkColor[0]+(1-s)*mtnShadColor[0],s*pineTrunkColor[1]+(1-s)*mtnShadColor[1],s*pineTrunkColor[2]+(1-s)*mtnShadColor[2]);
      out.strokeWeight(1);
      out.line(w1,h1,w/2,int(h*hf1*(0.8+random(0.2))));
      out.noStroke();
    }
    out.triangle(w1,h1,w2,h2,w3,h3);
  }
  
  wiggle(out,(out.width*out.height)/100+int(random((out.width*out.height)/100)),0.5);
  
  out.endDraw();
  return out;
}

class BigTree {
  int K = 3000;
  float l = 0.05;
  
  float buffer = 0.4;
  
  float rootScale = 0.05;
  int leafWeight = 2;
  float shift = 0.5;
  int k = 5;
  float tubePow = 1/2.7;
  float kappa = -0.5;
  float sparklerProb = 0.0;
  float deadProb = 0.5;
  float wScale = 1.0;
  float barkScale = 0.5;
  int minStick = 16;
  
  float[] xc = new float[K];
  float[] yc = new float[K];
  float[] zc = new float[K];
  int[] parent = new int[K];
  int[] weight = new int[K];
  int np = 1;
  
  boolean dead = false;
  boolean sparkler = false;
  
  void newSpecies() {
    initColors();
    rootScale = 0.02 + random(0.01);
    leafWeight = 3+int(random(2));
    shift = random(0.75);
    k = int(random(5)+1);
    tubePow = 1/(2+random(1));
    kappa = 0.1-random(0.9);
    wScale = 0.5+random(1.0);
    barkScale = 0.5+0.4*random(1.0);
    sparklerProb = 0.25;
    deadProb = 0.25;
    parent[0] = -1;
    sparkler = (random(1.0) < sparklerProb);
    K = 1500 + int(random(1500));
  }
  
  float yMin = 0;
  void addPointQuad(float ix, float iy, float iz) {
    float minV = MAX_FLOAT;
    float minD = MAX_FLOAT;
    int idx = 0;
    for (int i = 0; i < np; i++) {
      float d = sqrt((xc[i] - ix)*(xc[i] - ix) + (yc[i] - iy)*(yc[i] - iy) + (zc[i] - iz)*(zc[i] - iz));
      float v = kappa*(yc[i] - iy) + d;
      if (v < minV) {
        minV = v;
        minD = d;
        idx = i;
      }
    }
    float vx = (ix - xc[idx])/minD;
    float vy = (iy - yc[idx])/minD - kappa;
    float vz = (iz - zc[idx])/minD;
    
    float lv = sqrt(vx*vx + vy*vy + vz*vz);
    vx /= lv;
    vy /= lv;
    vz /= lv;
    
    xc[np] = xc[idx] + l*vx;
    yc[np] = yc[idx] + l*vy;
    zc[np] = zc[idx] + l*vz;
    if (yc[np] < yMin) yMin = yc[np];
    if (2*xc[np] < yMin) yMin = 2*xc[np];
    if (-2*xc[np] < yMin) yMin = -2*xc[np];
    
    parent[np] = idx;
    weight[np] = 1;
    
    while(parent[idx] != -1) {
      weight[idx]++;
      idx = parent[idx];
    }
    
    np++;
  }
  
  void addRandPointQuad() {
    float n1 = wScale*sqrt(-2*log(random(1.0)))*cos(random(TWO_PI));
    float n2 = wScale*sqrt(-2*log(random(1.0)))*cos(random(TWO_PI));
    float n3 = 0;
    
    for (int i = 0; i < k; i++) {
      n3 += log(random(1.0));  
    }
    n3 /= k;
    n3 -= shift;
    
    addPointQuad(n1/2,n3,n2/2);
  }
  
  PImage drawTree(int pixelScale) {
    dead = (random(1.0) < deadProb);
    PGraphics out = createGraphics(pixelScale,pixelScale);
    out.noSmooth();
    out.beginDraw();
    np = 1;
    yMin = 0;
    for (int i = 0; i < K-1; i++){
      addRandPointQuad();
    }
    yMin -= buffer;
    
    if (!dead) {
      for (int i = 0; i < K-1; i++) {
        if ((weight[i] < 200) && (zc[i] < 0)) {
          out.stroke(backLeafColor[0] + int(random(32)) - int(random(32)),backLeafColor[1] + int(random(32)) - int(random(32)),backLeafColor[2] + int(random(32)) - int(random(32)),128);
          
          out.strokeWeight(leafWeight);
          out.point(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin));
        }
      }
    }
    
    for (int i = 1; i < K-1; i++) {
      if (weight[i] > minStick) {
        float w = (yc[i] - yc[parent[i]])/sqrt((xc[i] - xc[parent[i]])*(xc[i] - xc[parent[i]]) + (yc[i] - yc[parent[i]])*(yc[i] - yc[parent[i]]) + (zc[i] - zc[parent[i]])*(zc[i] - zc[parent[i]]));
        w = (3-w)/4;
        out.stroke(w*bushTrunkColor[0]*barkScale,w*bushTrunkColor[1]*barkScale,w*bushTrunkColor[2]*barkScale);
        out.strokeWeight(rootScale*pixelScale*pow((1.0*weight[i])/K,tubePow));
        out.line(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin),pixelScale/2+(1.0*pixelScale*xc[parent[i]])/(-yMin),pixelScale+(1.0*pixelScale*yc[parent[i]])/(-yMin));
        out.point(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin));
        
        out.stroke(w*bushTrunkColor[0],w*bushTrunkColor[1],w*bushTrunkColor[2]);
        out.strokeWeight(rootScale*pixelScale*pow((1.0*weight[i])/K,tubePow)*sqrt(2)/2);
        out.line(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin),pixelScale/2+(1.0*pixelScale*xc[parent[i]])/(-yMin),pixelScale+(1.0*pixelScale*yc[parent[i]])/(-yMin));
        out.point(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin));
      }
    }
    
    if (!dead) {
      for (int i = 0; i < K-1; i++) {
        if ((weight[i] < 200) && (zc[i] > 0)) {
          float w = (yc[i] - yc[parent[i]])/sqrt((xc[i] - xc[parent[i]])*(xc[i] - xc[parent[i]]) + (yc[i] - yc[parent[i]])*(yc[i] - yc[parent[i]]) + (zc[i] - zc[parent[i]])*(zc[i] - zc[parent[i]]));
          w = (1+w)/2;
          if (sparkler) out.stroke(w*backLeafColor[0] + (1-w)*sparklerColor[0] + int(random(32)) - int(random(32)),w*backLeafColor[1] + (1-w)*sparklerColor[1] + int(random(32)) - int(random(32)),w*backLeafColor[2] + (1-w)*sparklerColor[2] + int(random(32)) - int(random(32)),128);
          else out.stroke(w*backLeafColor[0] + (1-w)*leafColor[0] + int(random(32)) - int(random(32)),w*backLeafColor[1] + (1-w)*leafColor[1] + int(random(32)) - int(random(32)),w*backLeafColor[2] + (1-w)*leafColor[2] + int(random(32)) - int(random(32)),128);
          
          out.strokeWeight(leafWeight);
          out.point(pixelScale/2+(1.0*pixelScale*xc[i])/(-yMin),pixelScale+(1.0*pixelScale*yc[i])/(-yMin));
        }
      }
    }
    
    wiggle(out,pixelScale*pixelScale/100,0.5,min(out.width-1,max(0,int(pixelScale/2+(1.0*pixelScale*xc[0])/(-yMin)))),min(out.height-1,max(0,int(pixelScale+(1.0*pixelScale*yc[0])/(-yMin)))));
    out.endDraw();
    
    return out;
  }
  
  // These are the colors that it uses.
  int[] leafColor = new int[3];
  int[] backLeafColor = new int[3];
  int[] bushTrunkColor = new int[3];
  int[] sparklerColor = new int[3];
  
  // This initializes the colors.  There are various rules relating them to try and make 
  // the image a little more coherent, and each color lives vaguely in a reasonable color space.
  void initColors() {
    float brt = random(0.5)+0.5;
    float shd = 1-brt;
    backLeafColor[0] = int(brt*0+shd*150);
    backLeafColor[1] = int(brt*0+shd*75);
    backLeafColor[2] = int(brt*0+shd*0);
    
    leafColor[1] = 128+int(random(128));
    leafColor[0] = int(random(leafColor[1]));
    leafColor[2] = int(random(leafColor[1]));
    
    brt = random(1.0);
    shd = 1-brt;
    bushTrunkColor[0] = int(brt*255+shd*150);
    bushTrunkColor[1] = int(brt*255+shd*75);
    bushTrunkColor[2] = int(brt*255+shd*0);
    
    sparklerColor[0] = int(random(64))+192;
    sparklerColor[1] = int(random(leafColor[0]));
    sparklerColor[2] = 64+int(random(leafColor[0]-64));
  }
}

//*** CONTACT PAPER ***
// This one is rare, but sometimes there is a contact paper oval it "peels of"
void makeContact() {
  PGraphics temp = createGraphics(in.width,in.height);
  temp.noSmooth();
  temp.beginDraw();
  temp.noStroke();
  temp.fill(255);
  temp.fill(0);
  temp.ellipse(in.width/2,in.height/2,in.width-10,in.height-10);
  temp.loadPixels();
  contactPaper.loadPixels();
  for (int i = 0; i < temp.pixels.length; i++) {
    if(temp.pixels[i] == 0) {
     contactPaper.pixels[i] = color(255);
    }
  }
  contactPaper.updatePixels();
  temp.updatePixels();
  temp.endDraw();
}

//*** IMAGE ROUTINES ***
PImage flip(PImage in) {
  PImage out = createImage(in.width,in.height,ARGB);  
    for (int i = 0; i < in.pixels.length; i++) {
      int x = i%in.width;
      int y = i/in.width;
      out.pixels[(in.width-1-x)+y*(in.width)] = in.pixels[i];
    }
  return out;
}

PImage flipV(PImage in) {
  PImage out = createImage(in.width,in.height,ARGB);  
    for (int i = 0; i < in.pixels.length; i++) {
      int x = i%in.width;
      int y = i/in.width;
      out.pixels[x+(in.height-1-y)*(in.width)] = in.pixels[i];
    }
  return out;
}

//*** DITHERING CODE ***
// Distance between colors
float distC(float cr, float cg, float cb, int i) {
  return (cr-r[i])*(cr-r[i])+(cg-g[i])*(cg-g[i])+(cb-b[i])*(cb-b[i]);
}

// This dithers using an intentionally coarse form error diffusion.  The idea behind making it coarse is
// to make happy accidents happen.  For instance, a tree that is green with a little red will not be drawn
// as a solid color, but instead as a green tree with the ocasional red fleck that resembles fruit
// or flowers.
void ditherIn() {
  in.loadPixels();
  float[] errorsR = new float[in.pixels.length];
  float[] errorsG = new float[in.pixels.length];
  float[] errorsB = new float[in.pixels.length];

  for (int i = 0; i < in.pixels.length; i++) {
    float curR = red(in.pixels[i]) + errorsR[i];
    float curG = green(in.pixels[i]) + errorsG[i];
    float curB = blue(in.pixels[i]) + errorsB[i];

    float minDist = MAX_FLOAT;
    int idx = 0;
    for (int j = 0; j < r.length; j++) {
      float curD = distC(curR, curG, curB, j);
      if (curD < minDist) {
        minDist = curD;
        idx = j;
      }
    }
    in.pixels[i] = color(r[idx], g[idx], b[idx]);
    float errR = curR - r[idx];
    float errG = curG - g[idx];
    float errB = curB - b[idx];

    if (i+1 < in.pixels.length) errorsR[i+1] += ditherFactor*errR/2.0;
    if (i+in.width < in.pixels.length) errorsR[i+in.width] += ditherFactor*errR/2.0; 

    if (i+1 < in.pixels.length) errorsG[i+1] += ditherFactor*errG/2.0;
    if (i+in.width < in.pixels.length) errorsG[i+in.width] += ditherFactor*errG/2.0; 

    if (i+1 < in.pixels.length) errorsB[i+1] += ditherFactor*errB/2.0;
    if (i+in.width < in.pixels.length) errorsB[i+in.width] += ditherFactor*errB/2.0;
  }
  in.updatePixels();
}

// This takes an input color and maps it into the space spanned by the Apple II
// color palette.  If you don't do this, the error diffusion has some problems...
void projToSpace() {
  in.loadPixels();
    int L = numProj;

  for (int i = 0; i < in.pixels.length; i++) {
    float errorsR = 0;
    float errorsG = 0;
    float errorsB = 0;
  
    float totR = 0, totG = 0, totB = 0;
    
    for (int l = 0; l < L; l++) {
      float curR = red(in.pixels[i]) + errorsR;
      float curG = green(in.pixels[i]) + errorsG;
      float curB = blue(in.pixels[i]) + errorsB;
  
      float minDist = MAX_FLOAT;
      int idx = 0;
      for (int j = 0; j < r.length; j++) {
        float curD = distC(curR, curG, curB, j);
        if (curD < minDist) {
          minDist = curD;
          idx = j;
        }
      }
      totR += r[idx];
      totG += g[idx];
      totB += b[idx];
      errorsR = curR - r[idx];
      errorsG = curG - g[idx];
      errorsB = curB - b[idx];
      
    }
    in.pixels[i] = color(int(totR/L),int(totG/L),int(totB/L));
  } 
  in.updatePixels();
}

// An alternate way to project into the desired color space.  It has the slight benefit
// that every color is made by mixing just two colors, and thus the dithering is more regular
void projToLines() {
 in.loadPixels();

 for (int i = 0; i < in.pixels.length; i++) {
   float minProj = MAX_FLOAT;
   int minR = 0;
   int minG = 0;
   int minB = 0;
   
   for (int ci = 1; ci < r.length; ci++) {
     for (int cj = 0; cj < ci; cj++) {
        float vr = r[ci] - r[cj];
        float vg = g[ci] - g[cj];
        float vb = b[ci] - b[cj];
        float d = sqrt(vr*vr+vg*vg+vb*vb);
        vr /= d;
        vg /= d;
        vb /= d;
        
        float dr = red(in.pixels[i]) - r[cj];
        float dg = green(in.pixels[i]) - g[cj];
        float db = blue(in.pixels[i]) - b[cj];
        
        float ip = min(d,max(0,dr*vr + dg*vg + db*vb));
        
        int curR = min(255,max(0,int(ip*vr + r[cj])));
        int curG = min(255,max(0,int(ip*vg + g[cj])));
        int curB = min(255,max(0,int(ip*vb + b[cj])));
        
        float curProj = (curR - red(in.pixels[i]))*(curR - red(in.pixels[i])) + (curG - green(in.pixels[i]))*(curG - green(in.pixels[i])) + (curB - blue(in.pixels[i]))*(curB - blue(in.pixels[i]));
        
        if (curProj < minProj) {
          minProj = curProj;
          minR = curR;
          minG = curG;
          minB = curB;
        }
     }
   }

   in.pixels[i] = color(minR,minG,minB);
 } 
 in.updatePixels();
}

// Helper function for below
float twoPowFunc(float in, float p1, float p2) {
  float retVal = 0;
  float h = pow(0.5,p1)/(pow(0.5,p1-1)*p1) + pow(0.5,p2)/(pow(0.5,p2-1)*p2);
  if (in < 0.5) {
    retVal = pow(in,p1)/(pow(0.5,p1-1)*p1);
  }
  else {
    retVal = h - pow(1 - in,p2)/(pow(0.5,p2-1)*p2);
  }
  return retVal/h;
}

// Allows for global color mood modification
void twoPowGamma() {
  in.loadPixels();
  for (int i = 0; i < in.pixels.length; i++) {
    int nr = int(255*twoPowFunc(red(in.pixels[i])/255.0,gamma1,gamma2));
    int ng = int(255*twoPowFunc(green(in.pixels[i])/255.0,gamma1,gamma2));
    int nb = int(255*twoPowFunc(blue(in.pixels[i])/255.0,gamma1,gamma2));
    in.pixels[i] = color(nr,ng,nb);
  }
  in.updatePixels();
}

// The CDFs for histogram normalization per color channel (to get the crazy colors!)
int numBin = 31;
int[] cdfR = new int[numBin];
int[] cdfG = new int[numBin];
int[] cdfB = new int[numBin];

// Compute the CDFs
void buildHist() {
  in.loadPixels();
  
  cdfR = new int[numBin];
  cdfG = new int[numBin];
  cdfB = new int[numBin];
  
  for (int i = 0; i < in.pixels.length; i++) {
    float rv = (1.0+2*red(in.pixels[i]))/512.0;
    float gv = (1.0+2*green(in.pixels[i]))/512.0;
    float bv = (1.0+2*blue(in.pixels[i]))/512.0;
    for (int j = int((numBin-1)*rv)+1; j < numBin; j++) {
      cdfR[j]++;
    }
    for (int j = int((numBin-1)*gv)+1; j < numBin; j++) {
      cdfG[j]++;
    }
    for (int j = int((numBin-1)*bv)+1; j < numBin; j++) {
      cdfB[j]++;
    }
  }

  in.updatePixels();
}

// try to use a smoother estimate of the CDF;
float evalCDF(int[] cdf, float v) {
  int low = int((numBin-1)*v);
  float shift = (numBin-1)*v - int((numBin-1)*v);
  if (low == (numBin - 1)) {
    return 1;
  }
  else {
    return ((1-shift)*cdf[low] + shift*cdf[low+1])/in.pixels.length;
  }
}

// Perform the histogram equalization
void histEq() {
  in.loadPixels();
  
  for (int i = 0; i < in.pixels.length; i++) {
    in.pixels[i] = color(255.0*evalCDF(cdfR,red(in.pixels[i])/255.0),
                         255.0*evalCDF(cdfG,green(in.pixels[i])/255.0),
                         255.0*evalCDF(cdfB,blue(in.pixels[i])/255.0));
  }
  
  in.updatePixels();
}

int[] cdf = new int[256];

// Build the histogram to equalize the intensity range
void buildIntensityHist() {
  cdf = new int[256];
  
  in.loadPixels();
  
  for (int i = 0; i < in.pixels.length; i++) {
    int l = int((min(min(red(in.pixels[i]),green(in.pixels[i])),blue(in.pixels[i])) + max(max(red(in.pixels[i]),green(in.pixels[i])),blue(in.pixels[i])))/2);
    
    for (int j = l; j < 256; j++) {
      cdf[j]++;
    }
  }
  
  in.updatePixels();
}

// Equalize intensity range
void intensityHistEq() {
  in.loadPixels();
  
  for (int i = 0; i < in.pixels.length; i++) {
    int maxV = int(max(max(red(in.pixels[i]),green(in.pixels[i])),blue(in.pixels[i])));
    int minV = int(min(min(red(in.pixels[i]),green(in.pixels[i])),blue(in.pixels[i])));
    int l = int((minV + maxV)/2);
    
    float scaledR;
    float scaledG;
    float scaledB;
    if (l < 128) {
      scaledR = red(in.pixels[i])/(l+1.0);
      scaledG = green(in.pixels[i])/(l+1.0);
      scaledB = blue(in.pixels[i])/(l+1.0);
    }
    else {
      scaledR = 2 - (255.0-red(in.pixels[i]))/(256.0-l);
      scaledG = 2 - (255.0-green(in.pixels[i]))/(256.0-l);
      scaledB = 2 - (255.0-blue(in.pixels[i]))/(256.0-l);      
    }
    int l2 = (255*cdf[l])/in.pixels.length;
    if (l2 < 128) {
      scaledR = (1.0+l2)*scaledR;
      scaledG = (1.0+l2)*scaledG;
      scaledB = (1.0+l2)*scaledB;
    }
    else {
      scaledR = 255 - (256-l2)*(2-scaledR);
      scaledG = 255 - (256-l2)*(2-scaledG);
      scaledB = 255 - (256-l2)*(2-scaledB);      
    }
    in.pixels[i] = color(scaledR,scaledG,scaledB);
  }
  
  in.updatePixels();
}

// A soft blur to give things a dreamy quality
void gentleBlur() {
  PImage out = createImage(in.width,in.height,ARGB);
  
  in.loadPixels();
  out.loadPixels();
  
  float cf = 4.0;
  
  for (int i = 0; i < in.width; i++) {
    out.pixels[i] = in.pixels[i];
    out.pixels[i+in.width*(in.height-1)] = in.pixels[i+in.width*(in.height-1)];
  }
  
  for (int j = 0; j < in.height; j++) {
    out.pixels[j*in.width] = in.pixels[j*in.width];
    out.pixels[in.width-1+j*in.width] = in.pixels[in.width-1+j*in.width];
  }
  
  for (int i = 1; i < in.width-1; i++) {
    for (int j = 1; j < in.height-1; j++) {
    out.pixels[i+in.width*j] = color((red(in.pixels[(i-1)+in.width*(j+0)])+red(in.pixels[(i+1)+in.width*(j+0)])+red(in.pixels[(i+0)+in.width*(j-1)])+red(in.pixels[(i+0)+in.width*(j+1)])+cf*red(in.pixels[(i+0)+in.width*(j+0)]))/(4+cf),
                                     (green(in.pixels[(i-1)+in.width*(j+0)])+green(in.pixels[(i+1)+in.width*(j+0)])+green(in.pixels[(i+0)+in.width*(j-1)])+green(in.pixels[(i+0)+in.width*(j+1)])+cf*green(in.pixels[(i+0)+in.width*(j+0)]))/(4+cf),
                                     (blue(in.pixels[(i-1)+in.width*(j+0)])+blue(in.pixels[(i+1)+in.width*(j+0)])+blue(in.pixels[(i+0)+in.width*(j-1)])+blue(in.pixels[(i+0)+in.width*(j+1)])+cf*blue(in.pixels[(i+0)+in.width*(j+0)]))/(4+cf));
    } 
  }
  
  in.updatePixels();
  out.updatePixels();
  
  in = out;
}

// Select a random color palette for additional variation
void randPalette() {
  //switch (int(random(13))) {
  switch (13) {
    case 0: r = r1; g = g1; b = b1; sigIdx = sigIdx1; sigBak = sigBak1; break;
    case 1: r = r4; g = g4; b = b4; sigIdx = sigIdx4; sigBak = sigBak4; break;
    case 2: r = r7; g = g7; b = b7; sigIdx = sigIdx7; sigBak = sigBak7; break;
    case 3: r = r3; g = g3; b = b3; sigIdx = sigIdx3; sigBak = sigBak3; break;
    case 4: r = r2; g = g2; b = b2; sigIdx = sigIdx2; sigBak = sigBak2; break;
    case 5: r = r5; g = g5; b = b5; sigIdx = sigIdx5; sigBak = sigBak5; break;
    case 6: r = r6; g = g6; b = b6; sigIdx = sigIdx6; sigBak = sigBak6; break;
    case 7: r = r8; g = g8; b = b8; sigIdx = sigIdx8; sigBak = sigBak8; break;
    case 8: r = r9; g = g9; b = b9; sigIdx = sigIdx9; sigBak = sigBak9; break;
    case 9: r = r10; g = g10; b = b10; sigIdx = sigIdx10; sigBak = sigBak10; break;
    case 10: r = r11; g = g11; b = b11; sigIdx = sigIdx11; sigBak = sigBak11; break;
    case 11: r = r12; g = g12; b = b12; sigIdx = sigIdx12; sigBak = sigBak12; break;
    case 12: r = r13; g = g13; b = b13; sigIdx = sigIdx13; sigBak = sigBak13; break;
    case 13: r = r14; g = g14; b = b14; sigIdx = sigIdx14; sigBak = sigBak14; break;
  }
}

// These are the colors that it uses.
int[] skyColor = new int[3];
int[] cloudColor = new int[3];
int[] sunColor = new int[3];
int[] mtnLightColor = new int[3];
int[] mtnShadColor = new int[3];
int[] grassColor = new int[3];
int[] dirtColor = new int[3];
int[] waterLineColor = new int[3];
int[] leafColor = new int[3];
int[] backLeafColor = new int[3];
int[] sparklerColor = new int[3];
int[] bushTrunkColor = new int[3];
int[] pineTrunkColor = new int[3];
int[] pineLeafColor = new int[3];
int[] pineBackLeafColor = new int[3];
int[] bushColor = new int[3];

// Color Modification parameters
float gamma1 = 1.0;            // Power near zero
float gamma2 = 1.0;            // Power near one 

// This initializes the colors.  There are various rules relating them to try and make 
// the image a little more coherent, and each color lives vaguely in a reasonable color space.
void initColors() {
  randPalette();
  
  gamma1 = random(0.8) + 0.8;
  gamma2 = random(0.2) + 0.8;
  
  skyColor[0] = int(random(128));
  skyColor[2] = int(random(256));
  skyColor[1] = int(random(max(skyColor[2],skyColor[0])));
  
  sunColor[0] = 255-int(random(128));
  sunColor[1] = min(255-int(random(128)),sunColor[0]);
  sunColor[2] = 255-int(random(128));
  
  cloudColor[0] = sunColor[0];
  cloudColor[1] = sunColor[1];
  cloudColor[2] = sunColor[2];
  
  float brt = random(1.0);
  float shd = 1-brt;
  mtnLightColor[0] = int(brt*255+shd*150);
  mtnLightColor[1] = int(brt*255+shd*75);
  mtnLightColor[2] = int(brt*255+shd*0);
  
  mtnShadColor[0] = (skyColor[0] + 255)/2;
  mtnShadColor[1] = (skyColor[1] + 255)/2;
  mtnShadColor[2] = (skyColor[2] + 255)/2;
  
  grassColor[1] = 128+int(random(128));
  int frac = int(random(grassColor[1]));
  grassColor[0] = int(frac*sqrt(random(1.0)));
  grassColor[2] = frac - grassColor[0];
  
  brt = random(0.75);
  shd = 1-brt;
  dirtColor[0] = int(brt*0+shd*150);
  dirtColor[1] = int(brt*0+shd*75);
  dirtColor[2] = int(brt*0+shd*0);
  
  waterLineColor[0] = 192+int(random(64));
  waterLineColor[1] = 192+int(random(64));
  waterLineColor[2] = 255;
  
  brt = random(1.0);
  shd = 1-brt;
  backLeafColor[0] = int(brt*0+shd*150);
  backLeafColor[1] = int(brt*0+shd*75);
  backLeafColor[2] = int(brt*0+shd*0);
  
  leafColor[1] = 128+int(random(128));
  leafColor[0] = int(random(leafColor[1]));
  leafColor[2] = int(random(leafColor[1]));
  
  sparklerColor[0] = int(random(128))+128;
  sparklerColor[1] = int(random(leafColor[0]));
  sparklerColor[2] = int(random(leafColor[0]));
  
  brt = random(1.0);
  shd = 1-brt;
  bushTrunkColor[0] = int(brt*255+shd*150);
  bushTrunkColor[1] = int(brt*255+shd*75);
  bushTrunkColor[2] = int(brt*255+shd*0);
  
  brt = random(1.0);
  shd = 1-brt;
  pineBackLeafColor[0] = int(brt*0+shd*150);
  pineBackLeafColor[1] = int(brt*0+shd*75);
  pineBackLeafColor[2] = int(brt*0+shd*0);
  
  pineLeafColor[1] = 128+int(random(128));
  pineLeafColor[0] = int(random(leafColor[1]));
  pineLeafColor[2] = int(random(leafColor[1]));
  
  brt = random(1.0);
  shd = 1-brt;
  pineTrunkColor[0] = int(brt*255+shd*150);
  pineTrunkColor[1] = int(brt*255+shd*75);
  pineTrunkColor[2] = int(brt*255+shd*0);
  
  bushColor[1] = grassColor[1];
  bushColor[0] = grassColor[0];
  bushColor[2] = grassColor[2];
}

// These are various shared parameters about how things are generated.
int waterShift;
int horizon;
int sunShift;
int cloudWiggle;
float cloudAR;
float deadProb;
float sparklerProb;
float pinePercent;
int treeNum;
float grassRange;
float bushiness;

float genSun;
float genMtn;
float genCloud;
float genContact;
float genFinal;
float genFarTrees;
float ditherFactor;
float reflProb;
boolean normalizeProb;
boolean dreamySwitch;
boolean intensityProb;

int curSeed;

// this initializes the global parameters
void initParameters() {
  addSig = false;
  randomSeed(newSeed);
  
  cloudAR = 0.02+random(0.08);
  waterShift = int(random(in.width/2)) - int(random(in.width/2));
  sunShift = int(random(64));
  horizon = int(random(48))-16;
  cloudWiggle = 750+int(random(250));
  deadProb = random(1.0);
  sparklerProb = random(1.0);
  pinePercent = max(min(random(4.0)-2,1),0);
  treeNum = int(random(256));
  grassRange = 0.3+random(0.2);
  bushiness = 0.5*min(int(random(4)),1);
  
  genSun = random(1.0);
  genMtn = random(1.0);
  genCloud = random(1.0);
  genContact = random(1.0);
  genFinal = random(1.0);
  genFarTrees = random(1.0);
  
  ditherFactor = 1.0;
  globalRandScale = sqrt(random(9.0/16.0));
  
  reflProb = random(1.0);
  normalizeProb = (random(1.0) < 0.75);
  dreamySwitch = true;
  intensityProb = (random(1.0) < 0.5);
}

// Initialize the output gif.
void initOut() {
  outputGif = new GifMaker(this, newSeed + ".gif");
  outputGif.setRepeat(0);
  outputGif.setDelay(2000/repFrame);
}

// This blends everything together.  Phase controls what layers to draw.  The string lastStep
// contains the Ross saying for the last step it drew.
int phase = -1;
String lastStep;
void finalMix() {
  // BLEND IT TOGETHER
  in.blend(water,0,0,water.width,water.height,0,0,in.width,in.height,BLEND);
  in.blend(front,0,0,front.width,front.height,0,0,in.width,in.height,BLEND);
  in.blend(sun,0,0,sun.width,sun.height,front.width/2 + waterShift-sun.width/2,int(0.5*in.height)+horizon-sun.height/2-sunShift,sun.width,sun.height,ADD);
  in.blend(clouds,in.width/2,in.height/2,in.width,in.height,0,0,in.width,in.height,DODGE);
  in.blend(reflCloud,in.width/2,in.height/2,in.width,in.height,0,2*horizon,in.width,in.height,DODGE);
  in.blend(mountains,in.width/2,in.height/2,in.width,in.height,0,0,in.width,in.height,BLEND);
  in.blend(reflMtn,in.width/2,in.height/2,in.width,in.height,0,2*horizon,in.width,in.height,BLEND);
  in.blend(farTrees,0,0,farTrees.width,farTrees.height,0,0,in.width,in.height,BLEND);
  in.blend(grassyAreas,0,0,water.width,water.height,0,0,in.width,in.height,BLEND);
  in.blend(finalTree,0,0,water.width,water.height,0,0,in.width,in.height,BLEND);
  
  // FILTERS
  if (phase == -1) buildHist();
  
  twoPowGamma();
  
  if (normalizeProb) histEq();
  if (phase == -1) buildIntensityHist();
  if (intensityProb) intensityHistEq();
  
  if (projLines) projToLines();
  else projToSpace();
  
  // MAYBE CONTACT PAPER AFTER (TO KEEP IT WHITE)
  in.blend(contactPaper,0,0,water.width,water.height,0,0,in.width,in.height,BLEND);
  
  if (dreamySwitch) gentleBlur();
 
  if (ditherSwitch) ditherIn();
  
  scaleIm();
  image(disp, 0, 0, disp.width, disp.height);
  
  if(addSig) {
    fill(r[sigBak],g[sigBak],b[sigBak]); 
    text("Ross",psf*6,disp.height-psf*4);
    text("Ross",psf*4,disp.height-psf*4);
    text("Ross",psf*5,disp.height-psf*3);
    text("Ross",psf*5,disp.height-psf*5);
    fill(r[sigIdx],g[sigIdx],b[sigIdx]); 
    text("Ross",psf*5,disp.height-psf*4);
  }
  fill(255);
  text(lastStep, 5*psf, disp.height+10*psf);
  fill(16);
  text(newSeed+"-9", 5*psf, disp.height+15*psf);
  
  //look away! you don't need to see this!
  copy(0,0,psf*233,height,psf*10,0,psf*233,height);
  fill(255);
  noStroke();
  rect(0,0,psf*10,height);
  rect(psf*243,0,psf*10,height);
  
  for (int i = 0; i < repFrame; i++) {
    outputGif.addFrame();
  }
  
  if (autoMode && iter) {phase++;}
  if (autoMode && addSig && (phase != 0)) {
    outputGif.finish();
    
    if (post) {
      boolean worked = true;
      try {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey("***")
          .setOAuthConsumerSecret("***")
          .setOAuthAccessToken("***")
          .setOAuthAccessTokenSecret("***");
        Configuration configuration = cb.build();
        OAuthAuthorization auth = new OAuthAuthorization(configuration);
        ImageUpload uploader = new ImageUploadFactory(configuration).getInstance(auth);
        
        File photo=new File(sketchPath(newSeed + ".gif"));
  
        uploader.upload(photo,"Happy Little Painting " + newSeed + "-9");
      }
      catch (TwitterException dontCare) {worked = false;}
      if (deleteOnceDone) {
        File photo=new File(sketchPath(newSeed + ".gif"));
        photo.delete();
      }
      
      if (worked) {
        exit();
      }
      else {
        phase = -1; newSeed ^= millis();
        initOut(); 
      }
    }
    else {
      if (deleteOnceDone) {
        File photo=new File(sketchPath(newSeed + ".gif"));
        photo.delete();
      }
      phase = -1; newSeed ^= millis();
      initOut(); 
    }
  }
}

// This shuffles an input array
void shuffle(String[] in) {
  for (int i = 0; i < in.length; i++) {
    int j = int(random(i+1));
    String temp = in[j];
    in[j] = in[i];
    in[i] = temp;
  }
}

// Shuffle to get random non-repeating lines
void shuffleLines() {
  shuffle(firstLines);
  shuffle(skyLines);
  shuffle(waterLines);
  shuffle(sunLines);
  shuffle(cloudLines);
  shuffle(mountainLines);
  shuffle(farTreeLines);
  shuffle(layerLines);
  shuffle(contactLines);
  shuffle(bigLines);
  shuffle(sigLines);
}

// Note what line to print
int firstPos = 0;
int skyPos = 0;
int waterPos = 0;
int sunPos = 0;
int cloudPos = 0;
int mountainPos = 0;
int farTreePos = 0;
int layerPos = 0;
int contactPos = 0;
int bigPos = 0;
int sigPos = 0;

// These all return the next line from the shuffled corpus
String nextFirst() {
  String out = firstLines[firstPos];
  firstPos = (firstPos+1)%firstLines.length;
  return out;
}

String nextSky() {
  String out = skyLines[skyPos];
  skyPos = (skyPos+1)%skyLines.length;
  return out;
}

String nextWater() {
  String out = waterLines[waterPos];
  waterPos = (waterPos+1)%waterLines.length;
  return out;
}

String nextSun() {
  String out = sunLines[sunPos];
  sunPos = (sunPos+1)%sunLines.length;
  return out;
}

String nextCloud() {
  String out = cloudLines[cloudPos];
  cloudPos = (cloudPos+1)%cloudLines.length;
  return out;
}

String nextMountain() {
  String out = mountainLines[mountainPos];
  mountainPos = (mountainPos+1)%mountainLines.length;
  return out;
}

String nextFarTree() {
  String out = farTreeLines[farTreePos];
  farTreePos = (farTreePos+1)%farTreeLines.length;
  return out;
}

String nextLayer() {
  String out = layerLines[layerPos];
  layerPos = (layerPos+1)%layerLines.length;
  return out;
}

String nextContact() {
  String out = contactLines[contactPos];
  contactPos = (contactPos+1)%contactLines.length;
  return out;
}

String nextBig() {
  String out = bigLines[bigPos];
  bigPos = (bigPos+1)%bigLines.length;
  return out;
}

String nextSig() {
  String out = sigLines[sigPos];
  sigPos = (sigPos+1)%sigLines.length;
  return out;
}

// These store the corpus
String[] firstLines;
String[] skyLines;
String[] waterLines;
String[] sunLines;
String[] cloudLines;
String[] mountainLines;
String[] farTreeLines;
String[] layerLines;
String[] contactLines;
String[] bigLines;
String[] sigLines;

// This initializes the corpus
void initLines() {
  firstLines = new String[] {
  "> I've started with a thin coat of liquid white.",
  "> Today we're going to do a painting I think you'll really enjoy!",
  "> I've got the canvas all wet, slick, and ready to go.",
  "> It's a beautiful day here, so lets have some fun!",
  "> Oh hi! I'm so excited today, so lets get right started.",
  "> Here we are again ready to do another fantastic painting!",
  "> Hi!  I'm glad you could join me today.",
  "> Ready to do a fantastic little painting?",
  "> Let's go up here and do it!",
  "> Hi, welcome back! So glad you could join me today.",
  "> I'd like to thank you for inviting me back in to your home.",
  "> I'm going to start out today with the old one inch brush.",
  "> This is such a super day, let's do a super little painting.",
  "> Let's just have some fun!"};
  
  skyLines = new String[] {
  "> Let the brush play and we can create a simple little sky.",
  "> We'll start by just putting a little color in the sky.",
  "> And just like that we have ourselves a simple little sky!",
  "> We'll start with out little X strokes and put in a happy sky.",
  "> Just let it pick up the white and as you work it gets lighter.",
  "> Isn't it fantastic you can make a sky this quick?",
  "> Use little Xs to just blend it all together.",
  "> Go up and drop in a happy little sky - just back and forth.",
  "> You can blend this to any degree of softness that you want.",
  "> Just make little criss-cross strokes like so.",
  "> Everything should get lighter twards the horizon.",
  "> And, that easy, we got a little color in the sky.",
  "> Use these little criss-cross strokes, they blend so easy."};
  
  waterLines = new String[] {
  "> Don't know if we'll have water, but put that same color below.",
  "> Water's like me. It's laaazy...",
  "> Just brush in some of that leftover color here on the bottom.",
  "> And maybe, maybe, we'll just put a little down across here.",
  "> We can just clean the brush here.  We don't care - whatever.",
  "> Maybe we'll have a little water. We can change our mind later."};
  
  sunLines = new String[] {
  "> Let's add a little indication of some sun zinging through!",
  "> I want a little light spot here in the sky.",
  "> Now we just want to put in a happy little glow.",
  "> We don't want to set the sky on fire! We just want a little glow.",
  "> Decide where your sun is going to live."};
  
  cloudLines = new String[] {
  "> Let's add some happy little clouds - they are so free!",
  "> Lets build a happy little cloud.",
  "> Let's just blend this little rascal here, ha! Happy as we can be.",
  "> Clouds are very, very free.",
  "> Just lightly blend the clouds, one hair and some air.",
  "> This would be a good sky to put a happy little cloud in!",
  "> Clouds are one of the free-est things in nature!",
  "> Maybe right in here lives a happy little cloud.",
  "> Very lightly, blend the entire sky.",
  "> Let's build us a happy little cloud that floats across the sky.",
  "> Think about a cloud.  Just float around and be there.",
  "> You can put the clouds up here with the 1 inch brush.",
  "> Just make a decision and drop him in.  Clouds are very free.",
  "> Using circular strokes, just fluff these little clouds.",
  "> Put a little bump on that cloud - give him some character!",
  "> Let's make some big fluffy clouds today.",
  "> Maybe there are some little stringy streaky clouds up here."};
  
  mountainLines = new String[] {
  "> You know me, let's make some almighty mountains!",
  "> You have unlimited power - you can literally move mountains!",
  "> Just pretend you're a whisper floating down the mountain.",
  "> I knew there was a big old mountain living up here somewhere.",
  "> It's your mountain, let light strike where you want it.",
  "> Each peak needs its own shadow to be its own individual.",
  "> Let's do a little mountain.",
  "> Let's do just a little ridge of mountains that lives back here.",
  "> You get the variations in color by not overmixing the paint.",
  "> You have to make a determination of where your light is from.",
  "> Make some big decisions - where is light hitting the mountain?",
  "> Tap the base of the mountain to add the indication of mist."};
  
  farTreeLines = new String[] {
  "> Tap in a few indications of some trees way off in the distance.",
  "> Maybe there are a few things waaaay off in the distance.",
  "> Pull down to add some very far away little trees.",
  "> Maybe you can just barely see some things happening waaay off.",
  "> All I'm doing is striking and pushing down.",
  "> It looks like there are hundreds and hundreds of trees.",
  "> Short tiny little strokes, the tiniest of strokes!"};
  
  layerLines = new String[] {
  "> You know when we do these paintings, they really aren't planned.",
  "> We learn to work with what happens!",
  "> Work in layers, it gives a tremendous sense of depth!",
  "> We don't make mistakes, just happy little accidents.",
  "> There's nothing wrong with having a tree as a friend.",
  "> I guess I'm a little weird. I like to talk to trees and animals.",
  "> Lets build some happy little trees.",
  "> That'll be our little secret.",
  "> In painting, you have unlimited power. You can bend rivers.",
  "> Remember our Golden Rule: A thin paint sticks to a thick paint.",
  "> And that makes it look like birch trees, isn't that sneaky?",
  "> Gotta give him a friend. Like I say 'everyone needs a friend'.",
  "> We don't know where it goes. We don't really care.",
  "> Any time ya learn, ya gain.",
  "> Any way you want it to be, that's just right.",
  "> Just put a few do-ers in there...",
  "> Decide where your little footy hills live.",
  "> Just scrape in a few indications of sticks and twigs in there.",
  "> Little raccoons and old possums 'n' stuff all live up in here.",
  "> Maybe in our world there lives a happy little tree over there.",
  "> Shwooop! You have to make those noises, or it doesn't work.",
  "> Trees cover up a multitude of sins.",
  "> You can put as many or as few as you want in your world.",
  "> You can do anything you want to do. This is your world.",
  "> Let's figure out where we'd like some little trees and stuff.",
  "> Think about where you'd like a tree, this is your creation.",
  "> Leave some airy spots in there, don't cover the whole canvas.",
  "> You need the dark in order to see the light!",
  "> Pull some of that same color down for instant reflections!",
  "> There you go!",
  "> Maybe we'll have some land back here.",
  "> Put a little highlight on there.",
  "> We want it to get a little darker as it gets closer to us.",
  "> Let this go with the lay of the land.",
  "> Always follow the lay of the land.",
  "> And maybe we can add another little projection over here.",
  "> Each projection needs his own personal reflection.",
  "> In this technique you need a paint that is very firm!",
  "> Just lay 'em in at random-we'll cover up what we don't want!",
  "> We'll begin just filling all this in here.",
  "> You always have all these sticks and things out in the woods.",
  "> You can create any kind of illusion you want on this canvas!",
  "> The contrast of light and dark gives eachother meaning.",
  "> We're not looking for a lot of detail, just some basic shapes.",
  "> It's the imperfections that make something beautiful!",
  "> Whatever makes you happy, you put in your world.",
  "> We have no limits.  We're only limited by our imagination!",
  "> No pressure.  Just relax and watch it happen.",
  "> Just layer after layer after layer.",
  "> Just do whatever makes you happy!",
  "> On this canvas you have total power.",
  "> These layers create the illusion of depth in your painting.",
  "> Each time you drop in another row, it creates another plane.",
  "> When painting with this method, you see things happening.",
  "> Maybe in our world, there lives a happy little evergreen.",
  "> Lets give him a little friend, everybody diserves a friend.",
  "> That old tree lives right in your brush - just push him out.",
  "> Let a little indication of some tree trunk show.",
  "> Put the indication of some highlights to make him stand out.",
  "> Don't kill all your dark! It is so important.",
  "> That little bit of color helps separate all of that.",
  "> This is a very free form of painting - no tracing, or drawing.",
  "> We just let these flow right out of our heart.",
  "> Learn a few basic rules and just let it happen.",
  "> All you need is a dream in your heart.",
  "> Maybe there is another little hill, we knew it was there!",
  "> Only guide is your heart. It takes you where you need to go.",
  "> It's your stream, your dirt, if you don't like it - change it!"};
  
  contactLines = new String[] {
  "> Let's have a little fun and peel off the contact paper.",
  "> Let's peel off that contact paper and see what we've got!",
  "> I'm going to take the paper off and let's take a look-see!"};
  
  bigLines = new String[] {
  "> You know me, I love those big trees!",
  "> Let's get crazy.",
  "> Little bird has to have a place to set there. There he goes...",
  "> You know me, I gotta put in a big tree.",
  "> Here's your bravery test!",
  "> Talk to the tree, make friends with it.",
  "> I think it's time to build an almighty tree.",
  "> Look at that!  Look at all that bark!",
  "> Oh let's get brave! Eeeenowwww! How's that?",
  "> Trees don't grow even, they grow however it makes them happy.",
  "> Maybe one goes right on off the canvas, we don't know where.",
  "> Make some big decisions!",
  "> Think about form and shape, and all those little limbs in there.",
  "> Pay attention to form and shape, that's what makes it special.",
  "> That was so much fun, let's make another one!"};
  
  sigLines = new String[] {
  "> It looks like we just about have a finished painting!",
  "> Well, the little clock on the wall says we're out of time.",
  "> I can't go over 30 minutes, because we have a mean ol' director.",
  "> Roll it in a little bright red and lets sign this right in here.",
  "> From all of us here I'd like to wish you happy painting!",
  "> Let's drop a little signature on here.",
  "> I think that makes a happy little painting.",
  "> That easy, you can complete a little scene.",
  "> We'll sign this painting right here."};
  
  firstPos = 0;
  skyPos = 0;
  waterPos = 0;
  sunPos = 0;
  cloudPos = 0;
  mountainPos = 0;
  farTreePos = 0;
  layerPos = 0;
  contactPos = 0;
  bigPos = 0;
  sigPos = 0;
}

// This draws all the layers.  Mostly it is just compositing the things generated above sometimes with more noise.
// The reason to apply noise after the composoting is to create coherent looking flow.  For instance, this makes
// the clouds appear as if they are all blown by the same wind etc.
void draw() {
  int step = 0;
  
  // PRIME THE CANVAS
  background(0);
  initParameters();
  initColors();
  initIn();
  
  initLines();
  shuffleLines();
  
  lastStep = nextFirst();
  if (phase == step) {finalMix(); return;}
  step++;
  
  // BUILD THE SKY
  PImage sky = skyGradient(2*in.width, in.height);
  front.blend(sky,0,0,2*in.width,in.height,-int(0.5*in.width),-int(0.5*in.height)+horizon,2*in.width,in.height,BLEND);
  lastStep = nextSky();
  if (phase == step) {finalMix(); return;}
  step++;

  // BUILD THE WATER
  PImage waterg = waterGradient(2*in.width, in.height);
  water.blend(waterg,0,0,2*in.width,in.height,-int(0.5*in.width),int(0.5*in.height)+horizon,2*in.width,in.height,BLEND);
  lastStep = nextWater();
  if (phase == step) {finalMix(); return;}
  step++;
  
  
  // ADD A SUN IF INTERESTED
  if (genSun < 0.1) {
    int sw = int(random(64))+32;
    sun = sunGradient(sw,sw);
    lastStep = nextSun();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // REFLECTION PARAMETERS
  boolean blurReflect = (random(1.0) < 0.5);
  int reflectWiggling = int(random(10000));
  
  // ADD A CLOUD IF INTERESTED
  if (genCloud < 0.75) {
    int num = int(random(500))+20;
    for (int i = 0; i < num; i++) {
      int sw = -int(log(random(1.0))*64)+1;
      int sh = int(cloudAR*sw)+1;
      PImage cloudIm = cloud(sw,sh);
      clouds.blend(cloudIm,0,0,cloudIm.width,cloudIm.height,int(random(2*in.width))-cloudIm.width/2,int(random(int(0.5*in.height)+horizon))-int(2*cloudIm.height)+in.height/2,cloudIm.width,cloudIm.height,BLEND);
    }
    clouds.filter(BLUR,int(random(2)));
    wiggle(clouds,5000+int(random(10000)),0.9+random(0.1));
    clouds.filter(BLUR,int(random(2)));
    
    // REFLECT IF DOING SO
    if (reflProb < 0.9) {
      reflCloud = flipV(clouds);
      wiggle(reflCloud,reflectWiggling,1.0,0,in.height-horizon);
      reflCloud.loadPixels();
      for (int i = 0; i < reflCloud.pixels.length; i++) {
      reflCloud.pixels[i] = color(red(reflCloud.pixels[i]),green(reflCloud.pixels[i]),blue(reflCloud.pixels[i]),alpha(reflCloud.pixels[i])/2);
      }
      reflCloud.updatePixels();
      if (blurReflect) reflCloud.filter(BLUR);
    }
    
    lastStep = nextCloud();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // ADD MOUNTAINS IF INTERESTED
  if (genMtn < 0.75) {
    int num = int(random(50))+100;
    float globalScale = random(0.35)+0.1;
    for (int i = 0; i < num; i++) {
      float mountainScale = globalScale*random(1.0);
      int w = int(pow(random(5),3))+1;
      PImage mtn = singlePeak(w,int(mountainScale*w)+1);
      mountains.blend(mtn,0,0,mtn.width,mtn.height,int(random(in.width))-mtn.width/2+in.width/2,horizon-mtn.height+in.height,mtn.width,mtn.height,BLEND);
    }
    wiggle(mountains,500+int(random(250)),1.0);
    
    // REFLECT IF DOING SO
    if (reflProb < 0.9) {
      reflMtn = flipV(mountains);
      wiggle(reflMtn,reflectWiggling,1.0,0,in.height-horizon);
      reflMtn.loadPixels();
      for (int i = 0; i < reflMtn.pixels.length; i++) {
        reflMtn.pixels[i] = color(red(reflMtn.pixels[i]),green(reflMtn.pixels[i]),blue(reflMtn.pixels[i]),alpha(reflMtn.pixels[i])/2);
      }
      reflMtn.updatePixels();
      if (blurReflect) reflMtn.filter(BLUR);
    }
    
    lastStep = nextMountain();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // ADD FAR TREES IF INTERESTED
  if (genFarTrees < 1.0) {
    PImage line = farTrees(in.width,7+2*int(random(8)));
    farTrees.blend(line,0,0,line.width,line.height,0,in.height/2+horizon-line.height/2,line.width,line.height,BLEND);
    lastStep = nextFarTree();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // ADD GRASSY AREAS
  int h = 20+int(random(15));
  int p = 0;
  float s = 2+random(5);
  float sf = 1.1+random(1.0);
  float mult = 0.1;
  int flipit = int(random(2));
  int centerShift = int(random(64))-int(random(64));
  float tv = random(0.5);
  
  while ((in.height/2+horizon-h/2+p < in.height) && (h<1.5*in.height)) {
    int th = int(h*(1-tv+random(2*tv)));
    PImage bushIm = grassyAreaWithPlantsAndBushes(244,th,mult);
    if (flipit == 0) grassyAreas.blend(bushIm,0,0,bushIm.width,bushIm.height,max(centerShift,0)+int(random(32*mult)),in.height/2+horizon-th/2+p-100,bushIm.width,bushIm.height,BLEND);
    if (flipit == 1) {
      bushIm = flip(bushIm);
      grassyAreas.blend(bushIm,0,0,bushIm.width,bushIm.height,min(centerShift,0)-int(random(32*mult)),in.height/2+horizon-th/2+p-100,bushIm.width,bushIm.height,BLEND);
    }
    
    p+=int(s);
    s = s*sf;
    h = int(h*sf);
    mult = min(0.9,mult*sf);
    flipit = 1-flipit;
    
    lastStep = nextLayer();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // MAYBE A FRONT SWOOP
  
  if (random(1.0) < 0.75) {
    treeNum = 0;
    bushiness = 0.5;
    flipit = int(random(2));
    int frontHeight = 100;
    PImage bushIm = grassyAreaWithPlantsAndBushes(244,frontHeight,mult);
    if (flipit == 1) bushIm = flip(bushIm);
    grassyAreas.blend(bushIm,0,0,bushIm.width,bushIm.height,0,in.height-frontHeight/2-90,bushIm.width,bushIm.height,BLEND);
    flipit = 1 - flipit;
    bushIm = grassyAreaWithPlantsAndBushes(244,frontHeight,mult);
    if (flipit == 1) bushIm = flip(bushIm);
    grassyAreas.blend(bushIm,0,0,bushIm.width,bushIm.height,0,in.height-frontHeight/2-90,bushIm.width,bushIm.height,BLEND);
    
    lastStep = nextLayer();
    if (phase == step) {finalMix(); return;}
    step++;
  
    // FINAL BIG TREE(S)
    if (genFinal < 0.75) {
      BigTree bigType = new BigTree();
      bigType.newSpecies();
      
      float totArea = random(3*in.height*in.height);
      float accArea = 0;
      while (accArea < totArea) {
       float shiftFrac = pow(random(1.0),2);
       int shift = int((in.width/4.0)*shiftFrac);
       int sc = int((1.5*sqrt(random(1.0))*(1-shiftFrac)+0.25)*in.height);
       accArea += sc*sc;
       PImage bigT = bigType.drawTree(sc);
       if (random(1.0) < 0.5) shift = in.width - shift;
       finalTree.blend(bigT,0,0,sc,sc,shift-sc/2,in.height-sc,sc,sc,BLEND);
       
       // ADD A LITTLE BUSH AT THE BOTTOM
       int bushSize = 20 + int(random(20));
       PImage footBush = littleBush(bushSize,bushSize,0.9);
       finalTree.blend(footBush,0,0,bushSize,bushSize,shift-bushSize/2,in.height-(3*bushSize)/4,bushSize,bushSize,BLEND);
        
       lastStep = nextBig();
       if (phase == step) {finalMix(); return;}
       step++;
      }
    }
  }
  
  // CONTACT PAPER SHAPES
  if (genContact < 0.01) {
    makeContact();
    lastStep = nextContact();
    if (phase == step) {finalMix(); return;}
    step++;
  }
  
  // SIGNATURE
  addSig = true;
  lastStep = nextSig();
  if (phase == step) {finalMix(); return;}
  step++;
  
  lastStep = "";
  finalMix();
}